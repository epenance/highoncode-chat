import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Card from './components/Card';
import MessageList from './components/MessageList';
import firebase from 'firebase';

class App extends Component {
  constructor() {
    super();

    this.messages = [];
    this.state = {
      formName: '',
      formMessage: '',
      messages: []
    }
  }

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyBRRYTsG8s-j3X77H1Q89axTXm82AhyYq8",
      authDomain: "webit-new.firebaseapp.com",
      databaseURL: "https://webit-new.firebaseio.com",
      storageBucket: "webit-new.appspot.com",
      messagingSenderId: "853452223776"
    };
    this.firebase = firebase.initializeApp(config);

    this.messageRef = this.firebase.database().ref('messages');

    this.messageRef.limitToLast(20).on('child_added', (snapshot) => {
      this.messages.push(snapshot.val());

      this.setState({
        messages: this.messages
      });
    });

  }

  onNameChange(name) {
    this.setState({
      formName: name
    });
  }

  onMessageChange(message) {
    this.setState({
      formMessage: message
    });
  }

  addNewMessage(event) {
    event.preventDefault();

    this.messageRef.push({
      author: this.state.formName,
      message: this.state.formMessage
    });

    this.setState({
      formMessage: '',
    });

  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to the Webit Chat</h2>
        </div>

        <Card>
          <form className="messageForm" onSubmit={(event) => this.addNewMessage(event)}>
            <h2>Send en besked</h2>
            <label>Dit navn</label>
            <input
              type="text"
              placeholder="Martin Hobert"
              id="formName"
              value={this.state.formName}
              onChange={(event) => this.onNameChange(event.target.value)}
              required
              />
            <label>Din besked</label>
            <textarea
              placeholder="Hej alle sammen ..."
              id="formMessage"
              value={this.state.formMessage}
              onChange={(event) => this.onMessageChange(event.target.value)}
              required
            >

            </textarea>
            <button>Send besked</button>
          </form>
        </Card>

        <MessageList room="WebitAarhus" messages={this.state.messages} />
      </div>
    );
  }
}

export default App;
