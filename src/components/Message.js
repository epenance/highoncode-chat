import React, { Component } from 'react';
import Card from './Card';

export default class Message extends Component {

  render() {
    return (
      <Card>
        <div className="message">
          <p>{this.props.message}</p>
        </div>
        <div className="author">
          {this.props.author}
        </div>
      </Card>
    )
  }
}
