import React, { Component } from 'react';
import _ from 'lodash';
import Card from './Card';
import Message from './Message';

export default class MessageList extends Component {

  renderMessages() {
    let messages = _.clone(this.props.messages);

    return messages.slice(Math.max(messages.length - 20, 0), messages.length).reverse().map((message, key) => {
      return (
          <Message key={key} message={message.message} author={message.author} />
         )
    });
  }

  render() {
    return (
      <Card>
        <h2>Chat: #{this.props.room}</h2>
        {this.renderMessages()}
      </Card>
    )
  }
}
